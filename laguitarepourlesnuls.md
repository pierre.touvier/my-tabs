# Lien vers la vidéo youtube

[La Guitare pour les nuls](https://www.youtube.com/watch?v=mtQo54BCfkE)

## Les temps forts de la vidéos

* les accords et explication: 57min
* riffs connus: 1h05
* gamme simple: 1h26
* gamme milieu de manche: juste apres
* exercice hammer on / pull off : 1h37
