# Tablature

Beaucoup d'information pour apprendre à jouer de la guitare:
[La guitare pour les nuls](./laguitarepourlesnuls.md)

[accord et gamme penta : liaison des deux](https://www.youtube.com/watch?v=lEtCo3qBi_M&ab_channel=GuitarCooktv)

## Basse

[Castle Of Glass](./Basse/Castle_Of_Glass.md)

## Guitare

[paint it black](https://www.youtube.com/watch?v=DIFleG8MgUw&ab_channel=GuitarZero2Hero)
[layla (unpluged)](https://www.youtube.com/watch?v=WHujjJEnZpI&ab_channel=GuitarZero2Hero)
[every breath you take](https://www.youtube.com/watch?v=3Ecx5eU8l7k&ab_channel=GuitarZero2Hero)
[feel good inc](https://tabs.ultimate-guitar.com/tab/gorillaz/feel-good-inc-tabs-200586)
[la grange (zz top)](https://www.youtube.com/watch?v=e8YlEwC3YxQ&ab_channel=MrGalagomusic)
https://www.youtube.com/watch?v=ocO46J8HcH0&ab_channel=GuitarTutorials